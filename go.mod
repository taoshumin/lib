module gitlab.com/taoshumin/lib

go 1.17

require (
	github.com/gogo/protobuf v1.3.2
	github.com/hashicorp/golang-lru v0.5.4
	github.com/minio/sha256-simd v1.0.0
	github.com/sasha-s/go-deadlock v0.3.1
	github.com/shirou/gopsutil/v3 v3.22.2
	github.com/syncthing/notify v0.0.0-20210616190510-c6b7342338d2
	golang.org/x/sys v0.0.0-20220330033206-e17cdc41300f
	golang.org/x/text v0.3.7
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/klauspost/cpuid/v2 v2.0.9 // indirect
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
)
